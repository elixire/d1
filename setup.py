#!/usr/bin/env python

from distutils.core import setup

setup(
    name="d1",
    version="1.0.0",
    packages=["d1"],
    install_requires=["aiohttp==3.8.1", "cryptography==2.3.1", "dnspython==1.16.0"],
    entry_points={"console_scripts": ["d1=d1:cli"]},
)
