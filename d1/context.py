import logging
import secrets
import json

import aiohttp
from aiohttp import ClientSession, ClientTimeout
from dns import resolver, exception
from cryptography.fernet import Fernet, InvalidToken

from d1.errors import DNSError, NameserverError, AddressLeak, QueryError

log = logging.getLogger(__name__)


def _mk_check_data():
    return secrets.token_urlsafe(32)


def _fmt(nslist):
    return " | ".join(nslist)


class Context:
    """Main d1 context."""

    def __init__(self, maplike_d1, maplike_unwanted_ips, *, session=None):
        self._raw_main = maplike_d1
        self._raw_unwanted = maplike_unwanted_ips

        self._domains = None
        self._fernet = None
        self._correct_ip = None

        timeout = ClientTimeout(total=20, connect=5, sock_connect=10, sock_read=10)
        self.session = session or ClientSession(timeout=timeout)

        self.resolver = resolver.Resolver()

    @property
    def instance_url(self):
        """Return the elixire instance url."""
        return self._raw_main.get("instance_url")

    @property
    def token(self):
        """Return the elixire API token."""
        return self._raw_main.get("token")

    @property
    def secret_key(self):
        """Return the secret key for d1's protocol."""
        return self._raw_main.get("secret_key")

    @property
    def webhook_url(self):
        """Return the webhook URL to give out errors to."""
        return self._raw_main.get("webhook")

    @property
    def nameservers(self):
        """Get a list of DNS nameservers that must be on wildcard domains."""
        return self._raw_main.get("nameservers", "").split(",")

    @property
    def unwanted_ips(self):
        """Return a dictionary of unwanted IP addresses."""
        res = {}

        for unwanted_ip, identifier in self._raw_unwanted.items():
            res[unwanted_ip] = identifier

        return res

    @property
    def fernet(self):
        """Get the Fernet encryption context."""
        if self._fernet is None:
            self._fernet = Fernet(self._raw_main.get("secret_key").encode())

        return self._fernet

    @property
    def ignore_ip_check(self) -> list:
        """Get the list of domains to be exempted from IP checking."""
        return self._raw_main.get("domain_ignore_ip_check", "").split(",")

    @property
    def ignore_ns_check(self) -> list:
        """Get the list of domains to be exempted from nameserver checking."""
        return self._raw_main.get("domain_ignore_ns_check", "").split(",")

    def __repr__(self):
        return (
            f"<Context instance={self.instance_url} " f"nameservers={self.nameservers}>"
        )

    async def _get(self, domain, path):
        full_url = f"https://{domain}{path}"
        async with self.session.get(full_url) as resp:
            return resp, await resp.json()

    async def _post(self, domain, path, **kwargs):
        full_url = f"https://{domain}{path}"
        async with self.session.post(full_url, **kwargs) as resp:
            return resp, await resp.json()

    @property
    async def domains(self):
        """Get the list of domains in an instance"""
        if self._domains is None:
            _resp, djson = await self._get(self.instance_url, "/api/domains")

            self._domains = {}

            domains = djson["domains"]

            # work with the ids in sorted order
            for domain_id in sorted(map(int, domains.keys())):
                self._domains[domain_id] = domains[str(domain_id)]

        return self._domains

    @property
    def wildcards(self):
        """Return all wildcard domains."""
        if self._domains is None:
            raise RuntimeError("domains list not initialized")

        return [
            (k, domain)
            for k, domain in self._domains.items()
            if domain.startswith("*.")
        ]

    async def close(self):
        """Close this context."""
        await self.session.close()

    def _query_domain(self, domain, query_type):
        try:
            log.debug("querying domain=%s qtype=%r", domain, query_type)
            return self.resolver.query(domain, query_type)
        except resolver.Timeout:
            return None
        except resolver.NXDOMAIN:
            raise DNSError("resolver sent NXDOMAIN", "misconfiguration")
        except resolver.NoAnswer:
            raise DNSError("resolver sent NoAnswer", "misconfiguration")
        except exception.DNSException as err:
            raise DNSError(
                f"resolver sent general error: {err!r}",
                "misconfiguration or upstream netfail",
            )

    async def check_ns(self, domain: str):
        """Check the nameservers for a given domain."""
        ns_result = self._query_domain(domain.replace("*.", ""), "NS")

        if ns_result is None:
            return

        # check the given response with
        # the set configuration ones

        # dnspython returns results in some weird result class
        # so we convert them to text
        nameservers = list(map(lambda x: x.to_text(), ns_result))

        for ns_text in nameservers:
            if ns_text not in self.nameservers:
                raise NameserverError(
                    "incorrect nameserver(s) detected",
                    f"set: {_fmt(nameservers)}\n" f"correct: {_fmt(self.nameservers)}",
                )

    async def check_addr_dns(self, domain: str):
        """Check the addreses for a domain, raising an error if
        the domain is showing an unwanted IP on its A records."""
        addr_res = self._query_domain(domain, "A")

        if addr_res is None:
            return

        for addr in addr_res:
            address = addr.to_text()

            ip_name = self.unwanted_ips.get(address)

            if ip_name is None:
                continue

            raise AddressLeak("unwanted ip leak", f"ip identifier: {ip_name}")

    async def _query_crypt(self, domain, payload):
        try:
            return await self._post(domain, "/api/check", json={"data": payload})
        except (
            json.JSONDecodeError,
            aiohttp.client_exceptions.ContentTypeError,
        ) as err:
            raise QueryError(
                f"Failed to extract json: {err!r}", "hijack? instance down?"
            )
        except Exception as err:
            raise QueryError(f"unable to post to check endpoint: {err!r}", "hijack?")

    def _encrypt(self, data: str) -> str:
        return self.fernet.encrypt(data.encode()).decode()

    def _decrypt(self, data: str) -> str:
        return self.fernet.decrypt(data.encode()).decode()

    def _validate_check_resp(self, payload, resp, rjson, *, bootstrap=False):
        pre = "[bootstrap] " if bootstrap else ""

        log.debug("resp: %r %r", resp.status, rjson)

        if resp.status != 200:
            raise QueryError(
                f"{pre}status not 200. got {resp.status}", "instance down?"
            )

        try:
            ciphertext = rjson["data"]
        except KeyError:
            raise QueryError(f"{pre}check res not dict" "api hijack")

        try:
            text = self._decrypt(ciphertext)
        except InvalidToken:
            raise QueryError(f"{pre}decryption failed", "ciphertext hijack")

        data_check, ip_address = text.split(",")

        if data_check != payload:
            raise QueryError(f"{pre}ciphertext tampered", "mitm")

        # return the given ip address if we don't have
        # a correct ip stored on us

        if not self._correct_ip:
            return ip_address

        if ip_address != self._correct_ip:
            raise QueryError("given ip != correct ip", "mitm")

    async def query_crypt(self, domain) -> str:
        """Query a domain using the d1 protocol.

        Returns the client IP address, from the point of view
        of the elixire instance.
        """
        payload = _mk_check_data()
        encrypted = self._encrypt(payload)

        resp, rjson = await self._query_crypt(domain, encrypted)

        return self._validate_check_resp(payload, resp, rjson)

    async def _bootstrap(self):
        """Fill the _correct_ip attribute."""
        # use the first domain in our disposal
        # to get the valid IP address that will be checked
        # against all other domains

        # d1 assumes the domain id 0 is unhijackable. this is
        # probably a bad assumption depending on your threat model.

        # TODO: maybe gather all ip addresses and calculate a majority
        # client ip address?
        self._correct_ip = await self.query_crypt(
            (await self.domains)[0].replace("*.", "")
        )

    async def validate(self, domain):
        """Validate a single domain following d1's protocol.

        Bootstraps a correct client IP address if it isn't
        already.
        """
        if self._correct_ip is None:
            await self._bootstrap()

        await self.query_crypt(domain)
